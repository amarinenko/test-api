# API для VGc

Для аутентификации используется токен, получаемый в ходе проверки пользователя по парольной паре. 
После успешной проверки и получения токена он должен передаваться в хедере X-AUTH-TOKEN.

## Регистрация и аутентификация

### 1. Регистрация пользователя
    
	URL: /rest/user/register
	
	Require Auth: нет
	
	Method: POST
	
	Request: { "nickname": "nick", "phone": "79099009090", "password":"password", "email":"email@email.com"}
	
	Required: nickname, phone, password
	
	Optional: email
	
	Response OK: {"result": true}
	
	Response Error: {"result": false, "message": "error message...."}

### 2. Проверка уникальности имени пользователя
  	
    URL: /rest/user/check
    
    Require Auth: нет
 
    Method: POST

    Request: { "nickname": "nick", "phone": "79099009090" }
    
    Required: nickname, phone    	

    Response OK: { "nickname": true/false, "phone": true/false }
    
### 3. Активация пользователя
    
	URL: /rest/user/activate
	
	Require Auth: нет
	
	Method: POST
	
	Request: {"phone": "79099009090", "code":"code"}

	Required: phone, code	
	
	Response OK: {"result": true}
	
	Response Error: {"result": false, "message": "error message...."}    
    
### 4. Аутентификация пользователя
 
#### Аутентификация с использованием телефона
 
    URL: /rest/user/login
    
    Method: POST
    
    Require Auth: нет

    Request: {"phone": "79099009090", "password": "password"}
    
    Required: phone, password    

    Response OK:
    Status: 200
    Headers:
	X-AUTH-TOKEN: token

    Response Error:
    Status: 401
    
#### Аутентификация с использованием никнейма
    
    URL: /rest/user/login
        
    Method: POST
    
    Require Auth: нет

    Request: {"phone": "79099009090", "password": "password"}
    
    Required: phone, password    

    Response OK:
    Status: 200
    Headers:
    X-AUTH-TOKEN: token

    Response Error:
    Status: 401

### 5. Получение информации о текущем залогиненном пользователе

	URL: /rest/user/current
    
    Method: GET
    
    Require Auth: да    
    
    Response OK: { "id": 1, "nickname":"nick", "phone": "79099009090", "email": "user@com.com", "contact_phone": "79035035050", "firstName": "first name", "lastName":"last name" }
	
	Response Error: {"result": false, "message": "error message...."}    

### 6. Поиск пользователей
	
	URL: /rest/user/search
    
    Method: POST

    Request: { "search": "7909900" }
    
    Require Auth: да    
    
    Response OK: [{ "id": 1, "nickname":"nick1", "phone": "79099009090", "email":"email@email.com", "contactPhone":"79035005050", "firstName": "first name", "lastName":"last name"}, { "id": 2, "nickname":"nick2", "phone": "79099009091", "email":"email1@email.com", "contactPhone":"79035005051", "firstName": "first name", "lastName":"last name"}, .... ]
	
	Response Error: {"result": false, "message": "error message...."}
